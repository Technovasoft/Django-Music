from django.contrib.auth.models import User
from django.views import generic
from .models import UserInfo, Comments
from music.models import Album, Song


# Create your views here.

class UserView(generic.DetailView):
    model = User
    slug_field = "username"
    template_name = "UComment/bio.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context

        context = super(UserView, self).get_context_data(**kwargs)

        # Agregar los contextos necesarios
        context['username'] = self.object.get_username()
        user = UserInfo.objects.filter(user=self.object)
        # print(user[0])
        coment = Comments.objects.filter(user=self.object)
        albums = Album.objects.filter(user=self.object)
        # print(coment)

        context['user'] = user[0]
        context['comments'] = coment
        context['albums'] = albums
        # context['songs'] = Song.objects.filter(album=Album.objects.filter(user=self.object))
        context['lalbums'] = len(albums)

        conti = 0
        for album in albums:
            cont = Song.objects.filter(album=album)
            conti += len(cont)

        context['lsongs'] = conti
        # print( Album.objects.filter(user=self.object))

        return context
