from django.contrib.auth.models import Permission, User
from django.db import models
from django.core.urlresolvers import reverse
#from django_markdown.models import MarkdownField


# Create your models here.

#userinfo Model
class UserInfo(models.Model):
    user = models.ForeignKey(User)
    Fname = models.CharField(max_length=500)
    Lname = models.CharField(max_length=500)
    ImageUser = models.FileField()
    description = models.CharField(max_length=500)

    def __str__(self):
        return self.user.get_username()


class Comments(models.Model):
    user = models.ForeignKey(User)
    Ucomen = models.CharField(max_length=500)
    content = models.CharField(max_length=500)
    created = models.DateField(auto_now_add=True, auto_now=False)

    def __str__(self):
        return self.user.get_username()



# #modelo del album
# class Album(models.Model):
#         user = models.ForeignKey(User, default=1)
#         artist = models.CharField(max_length=250)
#         album_title = models.CharField(max_length=500)
#         genre = models.CharField(max_length=100)
#         album_logo = models.FileField()
#         is_favorite  = models.BooleanField(default=False)
#
#         def get_absolute_url(self):
#             return reverse('music:detail',kwargs={'pk': self.pk})
#
#         def __str__(self):
#             return self.album_title + ' - ' +self.artist
