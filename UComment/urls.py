from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from . import views


app_name = 'UComment'
urlpatterns = [
    url(r'^profile/(?P<slug>\w+)/$', login_required(views.UserView.as_view(), login_url='music:index'), name='user'),
]
