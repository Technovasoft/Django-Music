from django.test import TestCase
from UComment.models import UserInfo


# Create your tests here.
class UserinfoTest(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.test_user = UserInfo(1, 'h', 'l', 'descripcion de un usuario')
        cls.test_user.save()
