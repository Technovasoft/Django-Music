from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from . import views

app_name = 'music'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),  # conexion con el index
    url(r'configuration/$', views.ConfigView, name = 'config'),  # pagina de configuracion
#    url(r'signup/$',views.SignView.as_view(),name = 'signup'),
    url(r'^(?P<pk>[0-9]+)/$', login_required(views.DetailView.as_view(), login_url='music:index'), name='detail'),  # conexion con el view de detalles del album
    url(r'album/add/$', login_required(views.AlbumCreate.as_view(), login_url='music:index'), name='album-add'),  # creacion de un nuevo album

     #para hacer un update se necesita ir tld/app/album/pk/
    url(r'album/(?P<pk>[0-9]+)/$', login_required(views.AlbumUpdate.as_view(), login_url='music:index'), name='album-update'), #actualizar un album
    url(r'album/(?P<pk>[0-9]+)/delete/$', login_required(views.AlbumDelete.as_view(), login_url='music:index'), name='album-delete'), #eliminar un album entero
    #url(r'register/$',views.RegisterView.as_view(),name='register'),
    url(r'^(?P<album_id>[0-9]+)/create_song/$', login_required(views.create_song, login_url='music:index'), name='create_song'), #crear cancion
    url(r'song/(?P<pk>[0-9]+)/delete/$', login_required(views.SongDelete.as_view(), login_url='music:index'), name='song-delete'), #eliminar una cancion
    url(r'^(?P<song_id>[0-9]+)/favorite/$', login_required(views.favorite,login_url='music:index'), name='favorite'),#favoritear un cancion
    url(r'^(?P<album_id>[0-9]+)/favorite_album/$', login_required(views.favorite_album, login_url='music:index'), name='favorite_album'),#favoritear un album
    ]
