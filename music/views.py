from django.views import generic
from .models import Album, Song
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.db.models import Q
from .forms import AlbumForm, SongForm
from django.contrib.auth import authenticate, login
from django.views.generic import View
from django.contrib.auth.decorators import login_required

AUDIO_FILE_TYPES = ['wav', 'mp3', 'ogg']
IMAGE_FILE_TYPES = ['png', 'jpg', 'jpeg']


class IndexView(View):

    def get(self, request):
        if not request.user.is_authenticated():
            return render(request, "base1.html", {})
        else:
            albums = Album.objects.filter(user=request.user)
            song_results = Song.objects.all()
            query = request.GET.get("q")
            if query:
                albums = Album.objects.all()
                albums = albums.filter(
                    Q(album_title__icontains=query) |
                    Q(artist__icontains=query) |
                    Q(genre__icontains=query)
                ).distinct()
                song_results = song_results.filter(
                    Q(song_title__icontains=query)
                ).distinct()
                return render(request, 'music/index.html', {
                    'albums': albums,
                    'songs': song_results,
                    'user': request.user,
                    'username': request.user.get_username(),
                })
            else:
                return render(request, 'music/index.html', {
                    'albums': albums,
                    'username': request.user.get_username(),
                })


def ConfigView(request):
    if not request.user.is_authenticated():
        return redirect('music:index')
    else:
        return render(request, 'music/config.html', {'username': request.user.get_username()})


class RegisterView(View):
    def get(self, request):
        if request.user.is_authenticated():
            return redirect('music:index')
        else:
            return render(request, 'music/signup.html', {'username': request.user.get_username()})


# class DetailViews(View):
#     def get(self,request):
#         if  not request.user.is_authenticated():
#             return redirect('http://127.0.0.1:8000')
#         else:
#             user = request.user
#             album = get_object_or_404(Album, pk=album_id)
#             return render(request, 'music/detail.html', {'album': album, 'user': user} )


class DetailView(generic.DetailView):
    model = Album
    template_name = 'music/detail.html'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(DetailView, self).get_context_data(**kwargs)
        context['username'] = self.object.user.get_username()

        return context


# Creates, Updates Y Deletes

class AlbumCreate(CreateView):
    model = Album
    fields = ['artist', 'album_title', 'genre', 'album_logo']

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AlbumCreate, self).get_context_data(**kwargs)
        # print(self.request.user.get_username())
        context['username'] = self.request.user.get_username()

        return context


class AlbumUpdate(UpdateView):
    model = Album
    fields = ['artist', 'album_title', 'genre', 'album_logo']

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(AlbumUpdate, self).get_context_data(**kwargs)
        context['username'] = self.object.get_username()

        return context


class AlbumDelete(DeleteView):
    model = Album
    success_url = reverse_lazy('music:index')


def create_song(request, album_id):
    form = SongForm(request.POST or None, request.FILES or None)
    album = get_object_or_404(Album, pk=album_id)
    if form.is_valid():
        albums_songs = album.song_set.all()
        for s in albums_songs:
            if s.song_title == form.cleaned_data.get("song_title"):
                context = {
                    'album': album,
                    'form': form,
                    'error_message': 'You already added that song',
                    'username': request.user.get_username(),
                }
                return render(request, 'music/create_song.html', context)
        song = form.save(commit=False)
        song.album = album
        song.audio_file = request.FILES['audio_file']
        file_type = song.audio_file.url.split('.')[-1]
        file_type = file_type.lower()
        if file_type not in AUDIO_FILE_TYPES:
            context = {
                'album': album,
                'form': form,
                'error_message': 'Audio file must be WAV, MP3, or OGG',
                'username': request.user.get_username(),
            }
            return render(request, 'music/create_song.html', context)

        song.save()
        return render(request, 'music/detail.html', {'album': album,
                                                     'username': request.user.get_username(), })
    context = {
        'album': album,
        'form': form,
        'username': request.user.get_username(),
    }
    return render(request, 'music/create_song.html', context)


# class SongCreate(CreateView):
#     model = Song
#     fields = ['artist','album_title','genre','album_logo']


class SongDelete(DeleteView):
    model = Song
    success_url = reverse_lazy('music:index')


# funciones de favoritear

def favorite(request, song_id):
    song = get_object_or_404(Song, pk=song_id)
    try:
        if song.is_favority:
            song.is_favority = False
        else:
            song.is_favority = True
        song.save()
    except (KeyError, Song.DoesNotExist):
        return redirect('http://127.0.0.1:8000/' + str(song.album.pk))
    else:
        return redirect('http://127.0.0.1:8000/' + str(song.album.pk))


def favorite_album(request, album_id):
    album = get_object_or_404(Album, pk=album_id)
    try:
        if album.is_favorite:
            album.is_favorite = False
        else:
            album.is_favorite = True
        album.save()
    except (KeyError, Album.DoesNotExist):
        return redirect('music:index')
    else:
        return redirect('music:index')
