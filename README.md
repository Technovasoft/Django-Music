# State

[![buildstatus](https://gitlab.com/Technovasoft/Django-Music/badges/master/build.svg)](https://gitlab.com/Technovasoft/Django-Music/commits/master)

# About
Una plataforma para poder subir musica a la nube y reproducirla como un sistema de
ayuda a los creadores independientes.


# Desing  

<img src="http://i.imgur.com/qMMchRJ.png" style="width:304px;height:228px;" />
<img src="http://i.imgur.com/By7e3RR.png" style="width:304px;height:228px;" />
<img src="http://i.imgur.com/XbNQJMY.png" style="width:304px;height:228px;" />
<img src="http://i.imgur.com/Ns93tDu.png" style="width:304px;height:228px;" />



## Requirements

Supported operating systems:

* Development environments: GNU/Linux, MacOS X, Windows.
* Production environments: GNU/Linux.

Requirements:

* virtualenv
* python == 3.6
* django == 1.11

## Getting the code

    $ git clone https://senjuana@gitlab.com/Technovasoft/Django-Music.git
    $ cd Django-Music

## Runing The code
Lo primero que tienes que hacer  una vez tienes el repositorio clonado es crear un entorno vitrual

    $ vitualenv DjangoM
    $ cd DjangoM

Despues de crear el entorno vitual toca activar este entorno

    $ source bin/activate

Una vez con el entorno activado procederemos a instalar los requerimientos necesarias para correr la app      
para hacer esto  nos movemos a la carpeta del repositorio.

    $ cd .. && cd Django-Music  
    $ pip install -r requirements.txt

Despues de instalar los requerimientos de la app lo unico que tienes que hacer es correr el servidor

    $ python manage.py runserver

Una vez el servidor de prueba este corriendo debes de entrar en el puerto local en el cual suele correr django.

http://127.0.0.1:8000/

## Contributing
Este codigo sigue nuestra version de Contributor Covenant antes de hacer aportes al codigo lee con atencion las reglas.
[Contributing](https://gitlab.com/Technovasoft/Django-Music/blob/master/CONTRIBUTING.md)

## License

MIT License

Copyright (c) 2017 Technovasoft

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

