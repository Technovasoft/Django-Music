var x = true;
var cont = 10;
var volAux = 10;
var volumenFlag = true;
var superAux = true;
var SUPER = "";
var flag = false;
var auxFlag = "";
var img_album = "";
var title="", artist="", album=""; 

function timeFunct(a){
	var duration_aux = Math.floor(document.getElementById(a).duration);
	var time_aux = Math.floor(document.getElementById(a).currentTime);
	var time_min = Math.floor(document.getElementById(a).currentTime/60);
	var time_sec = Math.floor(document.getElementById(a).currentTime%60);
	var duration_min = Math.floor(document.getElementById(a).duration/60);
	var duration_sec = Math.floor(document.getElementById(a).duration%60);

	document.getElementById('slider_time').max = parseInt(duration_aux);

	document.getElementById('tracktime').innerHTML = 
		(time_min>9 ? time_min : '0' + time_min) + ':' + (time_sec>9 ? time_sec : '0' + time_sec);
	document.getElementById('tracktimeOrigin').innerHTML = 
		(duration_min>9 ? duration_min : '0' + duration_min) + ':' + (duration_sec>9 ? duration_sec : '0' + duration_sec);

	document.getElementById('slider_time').value = parseInt(time_aux);

	if(duration_aux == time_aux){
		document.getElementById("playButton").className="glyphicon glyphicon-play";
		document.getElementById('tracktime').innerHTML ="00:00";
		document.getElementById('tracktimeOrigin').innerHTML ="00:00";
		document.getElementById('slider_time').value = parseInt("0");
		document.getElementById('slider_time').max = parseInt("0");
		$("#album_footer").attr('src', img_album);
		$("#album_info").attr('data-content', "");
		SUPER = "";
	}
}

function play_funct_aux(a, img, img_aux, title_aux, artist_aux, album_aux){
	//alert(a+", "+ img+", " +img_aux+", " +title_aux+", "+ artist_aux+", "+ album_aux);
	var info_var = "<b>Título: </b>"+title_aux+"<br><b>Aritsta: </b>"+artist_aux+"<br><b>Álbum: </b>"+album_aux;
	$("#album_info").attr('data-content', info_var);
	if(img!=""){
		img_album = img_aux;
		$("#album_footer").attr('src', img);
	}
    mediaFunctions(a);
}

function mediaFunctions(a){
	auxFlag = a;
	if(SUPER==""){
		SUPER = a;
		if(x){
			document.getElementById(a).play()
			document.getElementById("playButton").className="glyphicon glyphicon-pause";
			$('#media_button').attr('title', "pausar").tooltip("fixTitle");
			//$('#media_button').tooltip('show');
			x = false
		}else{
			document.getElementById(a).pause()
			document.getElementById("playButton").className="glyphicon glyphicon-play";
			$('#media_button').attr('title', "reproducir").tooltip("fixTitle");
			//$('#media_button').tooltip('show');
			x = true
		}
	}else{
		if(SUPER!=a){
			document.getElementById(SUPER).pause()
			document.getElementById("playButton").className="glyphicon glyphicon-play";
			$('#media_button').attr('title', "reproducir").tooltip("fixTitle");
			//$('#media_button').tooltip('show');
			x = true
			document.getElementById("playButton").className="glyphicon glyphicon-play";
			document.getElementById('tracktime').innerHTML ="00:00";
			document.getElementById('tracktimeOrigin').innerHTML ="00:00";
			document.getElementById('slider_time').value = parseInt("0");
			document.getElementById('slider_time').max = parseInt("0");
			document.getElementById(SUPER).currentTime = parseInt("0");

			SUPER = a;
			document.getElementById(SUPER).play()
			document.getElementById("playButton").className="glyphicon glyphicon-pause";
			$('#media_button').attr('title', "pausar").tooltip("fixTitle");
			//$('#media_button').tooltip('show');
			x = false
		}
	}

}

function mediaFunctionsPlus(){
	if(SUPER!=""){
		a = SUPER;
		auxFlag = a;
		if(x){
			document.getElementById(a).play()
			document.getElementById("playButton").className="glyphicon glyphicon-pause";
			$('#media_button').attr('title', "pausar").tooltip("fixTitle");
			$('#media_button').tooltip('show');
			x = false
		}else{
			document.getElementById(a).pause()
			document.getElementById("playButton").className="glyphicon glyphicon-play";
			$('#media_button').attr('title', "reproducir").tooltip("fixTitle");
			$('#media_button').tooltip('show');
			x = true
		}
	}
}


function getVol(){
	$('#slider_vol').attr('value', volAux.toString());
}

function vol(val){
	var c = val;
	volAux = c;
	document.getElementById(SUPER).volume = (c/10)

	if(document.getElementById(SUPER).volume > 0.5){
		document.getElementById("volume_button").className="glyphicon glyphicon-volume-up";
	}
	if(document.getElementById(SUPER).volume < 0.6){
		document.getElementById("volume_button").className="glyphicon glyphicon-volume-down";
	}
	if(document.getElementById(SUPER).volume == 0){
		document.getElementById("volume_button").className="glyphicon glyphicon-volume-off";
	}
}

$(document).ready(function(){
  $('#vol_btn').popover({ 
    html : true,
    content: function() {
      return $('#vol_popover').html();
    }
  });
});

$(document).on('click', function (e) {
    $('[data-toggle="popover"],[data-original-title]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {                
            (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
        }

    });
});
