# Levantamientos de requerimientos
En este documento estan los requerimientos funcionalesy no funcionales del proyecto

## Requerimientos Funcinales

* RF01--> El sistema debe permitir generar un registro del usuario
* RF02--> El sistema debe autentificar los nuevos usuarios mediante un link de identificacion de usuario
* RF03--> El sistema debe permitir el ingreso al sistema mediante cualquier dispisitivo con conexion a internet
* RF04--> El sistema debe permitir la subida de material musical ligado a cada usuario
* RF05--> El sistema debe permitir marcar material subido a la plataforma como favorito ya sea albums o canciones individuales
* RF06--> El sistema debe permitir actualizar la informacion de usuario en cualquier momento
* RF07--> El sistema debe permitir actualizar in informacion del material subido en cualquier momento
* RF08--> El sistema debe restringir la subida de archivos de audio a solo dos  formatos mp3 y flac
* RF09--> El sistema debe restringir que usuarios no vinculados a los materiales musicales puedan modificar de cualquier forma
* RF10--> El sistema debe restringir que usuarios sin estado de administracion puedan acceder a las opciones de administracion
* RF11--> El sistema debe permitir la creacion de usuarios con estado de administrador
* RF12--> El sistema debe permitir que un usuario cambie de estado de usuario normal a usuario administrador
* RF13--> El sistema debe permitir que un usuario administrador modifique estado de otros usuarios  
* RF14--> El sistema debe permitir a un usuario administrador tenga acceso a los logs del servidor
* RF15--> El sistema debe permitir a un usuario administrador tener acceso a la base de  datos
* RF16--> El sistema debe permitir a un usuario administrador hacer actividades de backup de la base de datos
* RF17--> El sistema debe permitir a un usuario administrador mandar mensajes masivos a los usuarios normales con fines de contacto
* RF18--> El sistema debe permitir a un usuario normal contactar con los usuarios administradores como medida de aclaraciones
* RF19--> El sistema debe permitir a un usuario administrador responder a las perticiones de los usuarios normales
* RF20--> El sistema debe restringir el acceso a la plataforma a los usuarios anonimos
* RF21--> El sistema debe restringir el acceso de los usuarios a los archivos del material musical de cualquuier usuario
* RF22--> El sistema debe autentificar la existencia de los links mandados por request al servidor
* RF23--> El sistema debe restringir el acceso a los links de media de del servidor a cualquier usuario
* RF24--> El sistema debe permitir avisar a los usuarios de un cambio en las politicas de privacidad
* RF25--> El sistema debe permitir subir las propias licencias de cada usuario para su trabajo siempre y cuando no tenga atribucion privativa
* RF26--> El sistema debe permitir hacer busquedas de cualquier tipo para encontrar material
* RF27--> El sistema debe poder hacer busquedas por etiquetas musicales
* RF28--> El sistema debe restringir la introduccion de caracteres maliciosos en los campos de texto

## Requerimientos No funcionales


* RNF01--> El sistema debe tener una disponibilidad del 100% de las veces que un usuario intente accederlo
* RNF02--> El tiempo para reiniciar el sistema no podraser mayor a 5 minutos
* RNF03--> El sistema sera desarrollado para cualquier plataforma mediante una web app
* RNF04--> El interfaz de usuario sera implementada unicamente con HTML5, JavaScript y python
* RNF05--> El procedimiento de desarrollo de software cumplira con los estandares de python dictados por  Barry Warsaw, y el w3d en cuestion de frotend
* RNF06--> La metodologia de desarrollo de backend sera Model Template View (MTV)
* RNF07--> El proceso de desarrrollo  de software  segestionara mediante git , utilizando gitlab como servidor de gestion de git
* RNF08--> El sistema debera tener un sistema de recupearacion de replicacion de la base de datos
* RNF09--> cada 90 dias el sistema debe de borrar los logs del servidor que afecte a los datos de conexion de los usuarios
* RNF10--> las IP address sera liberadas cada 5 años
* RNF11--> El sisitema se acatara las politicas de privacidad como un contrato mutuo pentre en sistemay los usuarios
* RNF12--> las pruebas de software sera realizadas en las herramientas de testeo pipelines,kubernetes mediante gitlab
* RNF13--> El sistema se acogera a las reglas de la lincecia MIT, es decir sera gratituo y codigo abierto, cualquiera podra tomar el proyecto e instlarlo en su propio servidor y cambiar el software, sin patentes y garantías
* RNF14--> El sistema debera cumplir con las leyes de igualdad para personas con discapacidad
* RNF15--> El sistema no revelara a sus operadores datos de los usuarios al menos que este estipulado en las politicas de privacidad
* RNF16--> El sistema debe mantener sistemas de seguridad contra Cross-site scripting
* RNF17--> El sistema debe mantener sistemas de seguridad contra non-persistent XSS
* RNF18--> El sistema debe mantener sistemas de seguridad contra persistent XSS
* RNF19--> El sistema debe mantener sistemas de restriccion de entrada no contextual de strings
* RNF2t --> El sistema debe mantener sistemas de restriccion de scripting
