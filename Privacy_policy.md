# Políticas de privacidad
## ¿Qué información colectamos?

La información que colectamos de ti cuando te registras en nuestro sitio y obtenemos data cuando tu participas
en la comunidad leyendo, escribiendo, y evaluando contenido compartido aquí.

Cuando te registras en nuestro sitio, tal vez te preguntemos que introduzcas tu nombre y tu cuenta de correo electrónico.
Tu cuenta de e-mail puede ser verificada por un correo que contiene un link único, si ese link es visitado 
sabremos que tú controlas la cuenta de e-mail que nos otorgaste.

Cuando te registras e interactuas en la pagina guardamos la IP address de donde fue originada la interaccion.
Tambien podemos conservar los logs del server que incluye cada una de las IP address de los 'request' en nuestro server.

## ¿Para qué utilizamos tu información?

Cualquier informacion que recolectamos de ti tal vez sea usada en una de las siguientes maneras:

* Para personalizar tu experencia - tu información nos ayuda a darle mejor respuestas a tus necesidades individuales.
* Para mejorar nuestro sitio - continuamente estamos mejorando nuestro sitio basandonos en tu feedback.
* Para mejorar nuestros servicios - tu informacion nos ayuda para poder ofrecer un mejor servicio y necesidades de nuestros servidores.
* Para enviarte e-mails periódicos - La cuenta de email que nos proporcionas tal vez sea utilizada para enviarte información, notificaciones acercade cambios e interacciones con su usuario y tu contenido compartido con la comunidad.

## ¿Cómo protegemos tu información?

Nosotros implementamos una variedad de medidas de seguridad para mantener a salvo tu información personal cuando entras, subes, o accedes a tu información personal.

## ¿Cúal es nuestra politica de retencion de datos?

Hacemos un gran esfuerzo en:

* Retenemos los logs del servidor que contienen todas las IP address de todas las peticiones en el servidor en un plazo no mayor a 90 dias.
* Retenemos las IP address asociadas con  usuarios registrados y sus post en un plazo no mayor a 5 años.

## ¿Usamos cookies?

NO.

## ¿Revelamos alguna información a terceros?

No vendemos, comerciamos, o compartimos información indentificable personal. Esto no incluye terceros confiables
que nos asisten con sus servicios para la operación de nuestro sitio, siempre y cuando estos terceros esten 
deacuerdo de mantener esta información confidencial. También  nosotros revelaremos tu información cuando sea 
necesario para cumplir con la ley, para reforzar la validez de nuestras políticas, o protejer nuestros derechos,
o seguridad. Para usuarios no identificados, su información como visitantes tal vez sea utilizada con terceros 
en concepto de marketing, advertising u otros usos.

## Children's Online Privacy Protection Act Compliance

Our site, products and services are all directed to people who are at least 13 years old. 
If this server is in the USA, and you are under the age of 13,
per the requirements of COPPA (Children's Online Privacy Protection Act), do not use this site.

## Politicas de Privacidad Online

Estas políticas de privacidad sólo aplican para información recolectada a travez de nuestro sitio y no a información 
recolectada offline.

## Tu Concentimiento

Por el uso de nuestro sitio, tu consientes el uso de nuestras politicas de privacidad.

## Cambios de nuestras politicas

Si decidimos cambiar nuestras politicas de privacidad, postearemos estos cambios en esta pagina.

Este documento es CC-By-SA, con su ultima actualizacion en abril 23, 2017.








 







