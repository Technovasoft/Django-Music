from django.shortcuts import render


def privacy(request):
    return render(request, 'privacy.html', {})


def helpi(request):
    return render(request, 'help.html', {})


def license(request):
    return render(request, 'license.html', {})
